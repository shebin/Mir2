package com.example.she.lol.anim;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.she.lol.Actor;

import java.util.HashMap;

import com.example.she.lol.GameView;
import com.example.she.lol.Shot;

/**
 * Created by she on 2017/1/10.
 */

public class AttackEffectAnim {
    private long deltaTime;	//多长时间跳一帧
    private long dz;	//当前帧的计时器
    private long preTime;	//记录的上一帧的执行时间
    private HashMap bmpRes;
    private long startTime;
    private long totalTime; //cd时间

    public AttackEffectAnim(HashMap bmpRes){
        this.deltaTime = 50;	//多少毫秒跳一帧 8
        this.dz = 0;
        this.preTime = 0;
        this.bmpRes = bmpRes;
        //this.startTime = System.currentTimeMillis();
        this.totalTime = 0;
    }

    //把当前的画布传进来,并且把坐标传进来
    public void Run(Canvas c,  Actor a, Shot s){
        if (s.getCurM() == 0){
            //当是getCurM为0的时候，攻击结束，说明这是一次新的攻击，选择最近的表作为攻击对象
            this.startTime = System.currentTimeMillis();
            s.setX(s.getDefaultX());
            s.setY(s.getDefaultY());
            s.setTargetMonster();
        }
        String FLAG = "A_DEFAULT";
        Bitmap curImg = (Bitmap)bmpRes.get(FLAG);
        long curTime = System.currentTimeMillis();
        dz = curTime - preTime;
        totalTime = curTime - startTime;

        float deltaX = s.getX() - a.getX();
        float deltaY = s.getY() - a.getY();

        Paint p = new Paint();
        p.setColor(Color.WHITE);
        p.setTextSize(40);
        //碰撞检测，如果
        if (s.getM() != null){
            this.deltaTime = 80;
            if (!s.getHit()){
                c.drawBitmap(curImg, GameView.CenterX + deltaX, GameView.CenterY +deltaY , null);
                if (s.isCollision()){
                    a.doAttack();
                }
                //最后一次的攻击也先画出来
                if (dz > deltaTime){
                    s.setY(s.getNextY());
                    s.setX(s.getNextX());
                    s.setCurM(s.getCurM() + 1);
                    dz = 0;
                    preTime = curTime;
                }
            }
        }else{
            //c.drawText("W:" + curImg.getWidth() + ";H:" + curImg.getHeight(), GameView.CenterX + deltaX, GameView.CenterY +deltaY, p);
            if (!s.getHit()){
                c.drawBitmap(curImg, GameView.CenterX + deltaX, GameView.CenterY +deltaY , null);
                if (dz > deltaTime){
                    if (!(s.getShotDistance() >300)){
                        s.setY(s.getNextY());
                        s.setX(s.getNextX());
                        s.setCurM(s.getCurM() + 1);
                    }else{
                        //攻击超出范围，也算是命中了
                        s.setHit(true);
                    }
                    dz = 0;
                    preTime = curTime;
                }
            }

        }
        if (totalTime > 1000){
            s.setCurM(0);
            a.setAttactFinish(true);
        }
    }
}