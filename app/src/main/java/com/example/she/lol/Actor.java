package com.example.she.lol;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.she.lol.anim.CharAttackAnim;
import com.example.she.lol.anim.CharDieAnim;
import com.example.she.lol.anim.CharStandAnim;
import com.example.she.lol.anim.CharRunAnim;
import com.example.she.lol.anim.MagicAnim;

import java.util.HashMap;

/**
 * Created by she on 2017/2/6.
 */

public class Actor {
    private int x;
    private int y;
    private String direct;  //当前角色面向的方向：U,D,L,R
    private Boolean isRun;  //当前角色是否在移动
    private Boolean isAttact;   //是否正在攻击
    private Boolean isAttactFinish; //上一次的攻击是否完成
    private Boolean isDrawRange;    //是否显示攻击范围
    private Boolean isDie;
    private float hp = 1000;       //当前血量
    private float maxhp = 1000;    //最大血量
    private Boolean isFireMagic ;   //是不是正在释放魔法

    public Boolean getFireMagic() {
        return isFireMagic;
    }

    public void setFireMagic(Boolean fireMagic) {
        isFireMagic = fireMagic;
    }

    public Boolean getDie() {
        return isDie;
    }

    public void setDie(Boolean die) {
        isDie = die;
    }

    public Boolean getAttactFinish() {
        return isAttactFinish;
    }

    public void setAttactFinish(Boolean attactFinish) {
        isAttactFinish = attactFinish;
    }

    public static int h = 150; //角色的默认身高
    public static int w = 100;  //角色的默认身宽
    private CharRunAnim runAnim;    //运动动画
    private CharStandAnim standAnim;    //站立动画
    private CharAttackAnim attackAnim;  //攻击时人物动画
    private MagicAnim magicAnim;  //攻击效果动画
    private CharDieAnim charDieAnim;
    private Shot shot;
    private Bitmap magic1;
    private Bitmap magic2;
    private Bitmap magic3;
    private int curMagic;   //当前技能编号

    public int getCurMagic() {
        return curMagic;
    }

    public void setCurMagic(int curMagic) {
        this.curMagic = curMagic;
    }

    public Bitmap getMagic1() {
        return magic1;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getMaxhp() {
        return maxhp;
    }

    public void setMaxhp(float maxhp) {
        this.maxhp = maxhp;
    }

    public void setMagic1(Bitmap magic1) {
        this.magic1 = magic1;
    }

    public Bitmap getMagic2() {
        return magic2;
    }

    public void setMagic2(Bitmap magic2) {
        this.magic2 = magic2;
    }

    public Bitmap getMagic3() {
        return magic3;
    }

    public void setMagic3(Bitmap magic3) {
        this.magic3 = magic3;
    }

    HashMap shotRes = null;//(HashMap) bmpRes.get("ATTACKBMP");

    public MagicAnim getMagicAnim() {
        return magicAnim;
    }

    public void setMagicAnim(MagicAnim magicAnim) {
        this.magicAnim = magicAnim;
    }

    public Monster getTarMonster() {
        return tarMonster;
    }

    public void setTarMonster(Monster tarMonster) {
        this.tarMonster = tarMonster;
    }

    private Monster tarMonster; //正在攻击的怪物

    public Boolean getAttact() {
        return isAttact;
    }

    public void setAttact(Boolean attact) {
        isAttact = attact;
    }

    public CharAttackAnim getAttackAnim() {
        return attackAnim;
    }

    public void setAttackAnim(CharAttackAnim attackAnim) {
        this.attackAnim = attackAnim;
    }

    public CharRunAnim getRunAnim() {
        return runAnim;
    }

    public void setRunAnim(CharRunAnim runAnim) {
        this.runAnim = runAnim;
    }

    public CharStandAnim getStandAnim() {
        return standAnim;
    }

    public void setStandAnim(CharStandAnim standAnim) {
        this.standAnim = standAnim;
    }

    public Boolean getDrawRange() {
        return isDrawRange;
    }

    public void setDrawRange(Boolean drawRange) {
        isDrawRange = drawRange;
    }
    public Actor(HashMap bmpRes){
        this.isRun = false;
        this.isAttact = false;
        this.isAttactFinish = true;
        this.isDrawRange = false;
        this.isDie = false;
        this.hp= 1000;
        this.maxhp = 1000;
        this.curMagic = 0;
        this.isFireMagic = false;
        HashMap runRes = (HashMap) bmpRes.get("RUNMAP");
        this.runAnim = new CharRunAnim(runRes);
        HashMap standRes = (HashMap) bmpRes.get("STANDMAP");
        this.standAnim = new CharStandAnim(standRes);
        HashMap attackRes = (HashMap) bmpRes.get("ATTACKMAP");
        this.attackAnim = new CharAttackAnim(attackRes);
        HashMap magic1AnimRes = (HashMap) bmpRes.get("MAGICMAP1");
        this.magicAnim = new MagicAnim(magic1AnimRes);
        HashMap charDieRes = (HashMap) bmpRes.get("DIEMAP");
        this.charDieAnim = new CharDieAnim(charDieRes);

        shotRes = (HashMap) bmpRes.get("ATTACKBMP");
        this.magic1 = (Bitmap) bmpRes.get("MAGIC1");
        this.magic2 = (Bitmap) bmpRes.get("MAGIC2");
        this.magic3 = (Bitmap) bmpRes.get("MAGIC3");
        this.direct = "R";
    }

    public Boolean getRun() {
        return isRun;
    }

    public void setRun(Boolean run) {
        isRun = run;
    }

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Shot getShot() {
        return shot;
    }

    public void setShot(Shot shot) {
        this.shot = shot;
    }

    public void doAttack(){
        if (this.shot != null && this.shot.getM() != null){
            if (this.shot.getM().getHp() >=0){
                int tmpValue = this.shot.getM().getHp() - 100;
                if (tmpValue <0){
                    tmpValue =0;
                    this.shot.getM().die();
                }
                this.shot.getM().setHp(tmpValue);
                this.shot.setHit(true);
            }
            //this.shot = null;
        }
    }

    public void createShot(float dftX, float defY, String dir){
        this.shot = new Shot(shotRes);
        this.shot.setDefaultX(dftX);
        this.shot.setDefaultY(defY);
        this.shot.setDirect(dir);
    }

    public void Run(Canvas c, float l, float t){
        if (isDie){
            charDieAnim.Run(c,this,l, t);
        }
        else{
            if (isFireMagic){
                if (isAttact){
                    this.attackAnim.Run(c,this,l, t);
                }else{
                    this.standAnim.Run(c,this,l, t);
                }
                magicAnim.Run(c,GameView.magicDir, GameView.magicStartX, GameView.magicStartY,GameView.magicOffsetX, GameView.magicOffsetY, l, t);
            }
            else if (isRun){
                runAnim.Run(c,this,l, t);
            }else if (isAttact) {
                this.attackAnim.Run(c, this, l, t);
            }
            else{
                standAnim.Run(c,this,l, t);
            }
            if (shot != null && !isAttactFinish){
                this.shot.getAttackEffectAnim().Run(c, this, this.shot);
            }

            if (isDrawRange){
                Paint p = new Paint();
                p.setStrokeWidth(2);
                p.setColor(Color.WHITE);
                p.setAlpha(20);
                c.drawOval(GameView.CenterX - 350 + Actor.w /2, GameView.CenterY - 350, GameView.CenterX + 350 + Actor.w /2, GameView.CenterY + 350,p);
            }
        }
    }

}
