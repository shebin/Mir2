package com.example.she.lol;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Message;

import com.example.she.lol.anim.MonAttackAnim;
import com.example.she.lol.anim.MonDieAnim;
import com.example.she.lol.anim.MonStandAnim;
import com.example.she.lol.anim.MonRunAnim;
import com.example.she.lol.comom.CollisionUtil;

import java.util.HashMap;

/**
 * Created by she on 2017/2/6.
 */

public class Monster {
    private int x;
    private int y;
    private int startX;
    private int startY;

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    private String direct;  //怪物的朝向：U,D,L,R
    private Boolean isRun;  //当前角色是否在移动

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public float getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(float attackRange) {
        this.attackRange = attackRange;
    }

    public float getAttentionRange() {
        return attentionRange;
    }

    public void setAttentionRange(float attentionRange) {
        this.attentionRange = attentionRange;
    }

    public float getMoveRange() {
        return moveRange;
    }

    public void setMoveRange(float moveRange) {
        this.moveRange = moveRange;
    }

    private int h; //角色的默认身高
    private int w;  //角色的默认身宽
    private Boolean isDie;
    private Boolean isSound;
    private float attackRange;  //攻击范围（进入这个范围就攻击，默认20）
    private float attentionRange;   //注意范围（进入这个范围就开始追击，默认300）
    private float moveRange;    //移动范围（中心点为怪物刷新点，默认400），脱离这个范围就托战
    private Boolean isBreak;  //是否脱离战斗

    public Boolean getDie() {
        return isDie;
    }

    public void setDie(Boolean die) {
        isDie = die;
    }

    public int getMaxhp() {
        return maxhp;
    }

    public void setMaxhp(int maxhp) {
        this.maxhp = maxhp;
    }

    private int maxhp = 1000;   //角色默认的最大血量，1000
    private int hp = 1000;   //角色默认的血量，1000

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Boolean getTarget() {
        return isTarget;
    }

    public void setTarget(Boolean target) {
        isTarget = target;
    }

    private Boolean isTarget;    //是否正在攻击的目标

    private MonRunAnim runAnim;    //运动动画
    private MonStandAnim standAnim;    //站立动画
    private MonDieAnim dieAnim; //死亡动画
    private MonAttackAnim attackAnim;

    public Monster(HashMap bmpRes){
        this.isTarget = false;
        this.isRun = false;
        this.hp = 1000;
        this.maxhp = 1000;
        this.isDie = false;
        this.isSound= false;
        this.attackRange = 120;
        this.attentionRange = 300;
        this.moveRange = 400;
        this.isBreak = false;
        this.h = 150;
        this.w = 120;
        HashMap runRes = (HashMap) bmpRes.get("RUNMAP");
        this.runAnim = new MonRunAnim(runRes);
        HashMap standRes = (HashMap) bmpRes.get("STANDMAP");
        this.standAnim = new MonStandAnim(standRes);
        HashMap dieRes = (HashMap) bmpRes.get("DIEMAP");
        this.dieAnim = new MonDieAnim(dieRes);
        HashMap attackRes = (HashMap) bmpRes.get("ATTACKMAP");
        this.attackAnim = new MonAttackAnim(attackRes);
        this.direct = "DR";
    }

    public Boolean getRun() {
        return isRun;
    }

    public void setRun(Boolean run) {
        isRun = run;
    }

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    //死亡方法，暂时还没写
    public void die(){
        this.setHp(0);
        this.isDie = true;
        this.setTarget(false);
    }

    public void PlaySoundById(int id){
        if (!isSound){
            Message msg = new Message();
            msg.arg1 = id;
            GameView.handler.sendMessage(msg);
            this.isSound = true;
        }
    }

    //得到当前怪物与actor之间的距离
    public double getActorDistance(){
        float _x = Math.abs(GameView.actor.getX() + Actor.w/2 - this.getX() - this.getW()/2);
        float _y = Math.abs(GameView.actor.getY() + Actor.h - this.getY() - this.getH());
        return Math.sqrt(_x*_x+_y*_y);
    }

    //得到与起始刷新点之间的距离(由于都是以怪物本身为参考，所以不需要考虑身体的偏移量)
    public double getStartDistance(){
        float _x = Math.abs(this.getX() - this.getStartX());
        float _y = Math.abs(this.getY() - this.getStartY());
        return Math.sqrt(_x*_x+_y*_y);
    }

    //根据当前角色，得到当前怪物的方向（思路是根据,x，y的偏移量来算，由于怪物和角色的身长都不一样，所以要以中心点来算，中心点横坐标为图片中央，纵坐标以底脚部为准）
    public String getDirByActor(){
        float mx = this.getX() + this.getW()/2;
        float my = this.getY() + this.getH();
        float ax = GameView.actor.getX() + Actor.w/2;
        float ay = GameView.actor.getY() + Actor.h;
        return CollisionUtil.getNextDirection(mx,my,ax,ay);
    }


    private String getDirByStartPos(){
        float mx = this.getX();
        float my = this.getY();
        float ax = this.getStartX();
        float ay = this.getStartY();
        return CollisionUtil.getNextDirection(mx,my,ax,ay);
    }

    public Boolean getBreak() {
        return isBreak;
    }

    public void setBreak(Boolean aBreak) {
        isBreak = aBreak;
    }

    public void Run(Canvas c, float l, float t){
        if (isDie){
            dieAnim.Run(c,this,l, t);
            PlaySoundById(2);
        }
        else if (isBreak){  //如果脱战，不管其他逻辑，先回到远点再说
            setDirect(getDirByStartPos());
            double ds = getStartDistance();
            runAnim.Run(c,this,l, t);
            if (ds <20){
                isBreak = false;
                isTarget = false;
            }
        }
        else if (isTarget){
            //范围大于攻击范围20的时候，就跑，范围小于攻击范围20的时候就攻击
            double d = getActorDistance();
            double ds = getStartDistance();
            if (d >this.attackRange){
                if (ds < this.moveRange){
                    //setDirect(getDirByActor());
                }else{
                    isBreak = true;
                }
                runAnim.Run(c,this,l, t);
            }
            else{
                attackAnim.Run(c,this,l, t);
            }

        }else{
            standAnim.Run(c,this,l, t);
        }
    }
}
