package com.example.she.lol;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.os.Handler;
import android.os.Message;

import com.example.she.lol.comom.CollisionUtil;
import com.github.jootnet.mir2.core.Texture;
import com.github.jootnet.mir2.core.image.*;
import com.github.jootnet.mir2.core.image.WIL;
import com.github.jootnet.mir2.core.map.Map;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by she on 2017/11/14.
 */

public class GameView extends SurfaceView implements Runnable, SurfaceHolder.Callback {
    private SurfaceHolder holder;
    private Canvas canvas;
    private Thread mainT;
    private Context context;
    private static final int ACTION_POINTER_2_MOVE = 0x00000107;    //自定义第二个手指MOVE的情况
    private  static boolean isRunning = false;
    static Handler handler;
    Map map = null;
    public static int loadPercenter = 0;
    public static Actor actor;
    //Tile 地图整个的块数
    public final static int TILE_WIDTH_COUNT = 50;
    public final static int TILE_HEIGHT_COUNT = 50;

    //数组元素为0则什么都不画
    public final static int TILE_NULL = 0;

    //tile块的宽高
    public final static int TILE_WIDTH = 94;
    public final static int TILE_HEIGHT = 64;

    //屏幕的高度和宽度
    public static int SWidth = 0;
    public static int SHeight = 0;
    public static int CenterX = 0;
    public static int CenterY = 0;

    //Tile 地图像素大小
    public final static int MAP_WIDTH = TILE_WIDTH_COUNT * TILE_WIDTH;
    public final static int MAP_HEIGHT = TILE_HEIGHT_COUNT * TILE_HEIGHT;

    //当前屏幕可以画的地图的高度和宽度的数量
    private static int SWCount = 0;
    private static int SHCount = 0;

    private static Bitmap bmpAttr = null;

    public static ArrayList<Monster> monsters = null;

    public static float magicOffsetX = 0;
    public static float magicOffsetY = 0;
    public static float magicStartX = 0;
    public static float magicStartY = 0;
    public static String magicDir = "D";

    //加载野怪动画资源
    private static void loadMonsters(SurfaceView v){
        monsters = new ArrayList<Monster>();
        //加载羊资源
        HashMap actorRes = loadMonster5Res(v, 1080);
        Monster m = new Monster(actorRes);
        m.setH(100);
        m.setW(128);
        m.setStartX(1566);
        m.setStartY(1100);
        m.setX(1566);
        m.setY(1100);
        monsters.add(m);
        loadPercenter = 40;

        //加载鹰资源
        HashMap actor1Res = loadMonster5Res(v, 1440);
        Monster m1 = new Monster(actor1Res);
        m1.setW(304);
        m1.setH(200);
        m1.setStartX(3040);
        m1.setStartY(1450);
        m1.setX(3040);
        m1.setY(1450);
        m1.setDirect("DL");
        monsters.add(m1);
        loadPercenter = 50;

        //加载鼠资源
        HashMap actor2Res = loadMonster5Res(v, 2160);
        Monster m2 = new Monster(actor2Res);
        m2.setH(194);
        m2.setW(204);
        m2.setStartX(1560);
        m2.setStartY(1750);
        m2.setX(1560);
        m2.setY(1750);
        m2.setDirect("UR");
        monsters.add(m2);
        loadPercenter = 60;

        //加载虫资源
        HashMap actor3Res = loadMonster5Res(v, 2880);
        Monster m3 = new Monster(actor3Res);
        m3.setH(110);
        m3.setW(128);
        m3.setStartX(3036);
        m3.setStartY(1970);
        m3.setX(3036);
        m3.setY(1970);
        m3.setDirect("UL");
        monsters.add(m3);
        loadPercenter = 70;

        //加载弓箭手资源
        HashMap actor4Res = loadMonster5Res(v, 2520);
        Monster m4 = new Monster(actor4Res);
        m4.setH(184);
        m4.setW(144);
        m4.setStartX(3424);
        m4.setStartY(814);
        m4.setX(3424);
        m4.setY(814);
        m4.setDirect("DL");
        monsters.add(m4);
        loadPercenter = 75;

        //加载电僵尸资源
        HashMap actor5Res = loadMonster5Res(v, 0);
        Monster m5 = new Monster(actor5Res);
        m5.setW(128);
        m5.setH(154);
        m5.setStartX(1180);
        m5.setStartY(2278);
        m5.setX(1180);
        m5.setY(2278);
        m5.setDirect("UR");
        monsters.add(m5);
        loadPercenter = 75;
    }

    public static void loadActor(SurfaceView v){
        sdCardRoot = Environment.getExternalStorageDirectory().getAbsolutePath();
        loadPercenter = 10;
        bmpAttr = scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.mipmap.att),0.5f) ;
        loadPercenter = 20;
        loadMap(v);
        loadMonsters(v);
        HashMap actorRes = loadActorRes(v);
        actor = new Actor(actorRes);
        actor.setX(MAP_WIDTH/2);
        actor.setY(MAP_HEIGHT/2);
    }
    private void init(){
        setFocusableInTouchMode(true);
        holder = this.getHolder();//这个this指的是这个Surface
        holder.addCallback(this); //这个this表示实现了Callback接口
        //loadActor();
    }
    public GameView(Context context) {
        super(context);
        this.setFocusable(true);
        this.context = context;
        init();
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context);
        this.setFocusable(true);
        this.context = context;
        init();
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context);
        this.setFocusable(true);
        this.context = context;
        init();
    }

    static String  sdCardRoot = "";

    private static HashMap loadResByDirect(String dir, int startNo, int nCount,  WIL w){
        HashMap ret = new HashMap();
        ArrayList<Bitmap> bps = new ArrayList<Bitmap>();
        ArrayList<ImageInfo> iis = new ArrayList<ImageInfo>();
        for(int i=startNo;i<startNo+nCount;i++){
            Texture img = w.tex(i);
            ImageInfo curImgInfo = w.info(i);
            iis.add(curImgInfo);
            Bitmap bmp = decodeFrameToBitmap(img.getRGBs(), img.getWidth(), img.getHeight());
            bps.add(scaleBitmap(bmp,(float)2));
        }
        ret.put(dir, bps);
        ret.put(dir+"_INFO", iis);
        return ret;
    }

    private static HashMap loadMonster5Res(SurfaceView v, int startNo){
        WIL.GLOBAL_ONLYWIL_MODE = true;
        HashMap aRes = new HashMap();
        //加载角色图片
        if (getMonster5FilePath() == ""){
            InputStream iFile = v.getResources().openRawResource(R.raw.mon5);
            writeData2SDCard("lol", "mon5.wil", iFile);
        }
        WIL wil = new WIL(sdCardRoot + File.separator + "lol" + File.separator + "mon5.wil");
        //站立动画
        HashMap standMap = new HashMap();
        //上(S_U)
        standMap.putAll(loadResByDirect("S_U",startNo,4,wil));
        //上右(S_UR)
        standMap.putAll(loadResByDirect("S_UR",startNo+10,4,wil));
        //右(S_RS)
        standMap.putAll(loadResByDirect("S_R",startNo+20,4,wil));
        //下右(S_DR)
        standMap.putAll(loadResByDirect("S_DR",startNo+30,4,wil));
        //下(U_D)
        standMap.putAll(loadResByDirect("S_D",startNo+40,4,wil));
        //下左(U_DL)
        standMap.putAll(loadResByDirect("S_DL",startNo+50,4,wil));
        //左(U_L)
        standMap.putAll(loadResByDirect("S_L",startNo+60,4,wil));
        //上左(U_UL)
        standMap.putAll(loadResByDirect("S_UL",startNo+70,4,wil));
        aRes.put("STANDMAP", standMap);

        //运动动画
        HashMap runMap = new HashMap();
        //上(U)
        runMap.putAll(loadResByDirect("R_U",startNo+80,6,wil));
        //上右(UR)
        runMap.putAll(loadResByDirect("R_UR",startNo+90,6,wil));
        //右(R)
        runMap.putAll(loadResByDirect("R_R",startNo+100,6,wil));
        //下右(DR)
        runMap.putAll(loadResByDirect("R_DR",startNo+110,6,wil));
        //下(D)
        runMap.putAll(loadResByDirect("R_D",startNo+120,6,wil));
        //下左(D)
        runMap.putAll(loadResByDirect("R_DL",startNo+130,6,wil));
        //左(L)
        runMap.putAll(loadResByDirect("R_L",startNo+140,6,wil));
        //上左(UL)
        runMap.putAll(loadResByDirect("R_UL",startNo+150,6,wil));
        aRes.put("RUNMAP", runMap);

        //攻击动画
        HashMap attackMap = new HashMap();
        //上(U)
        attackMap.putAll(loadResByDirect("A_U",startNo+160,6,wil));
        //上右(UR)
        attackMap.putAll(loadResByDirect("A_UR",startNo+170,6,wil));
        //右(R)
        attackMap.putAll(loadResByDirect("A_R",startNo+180,6,wil));
        //下右(DR)
        attackMap.putAll(loadResByDirect("A_DR",startNo+190,6,wil));
        //下(D)
        attackMap.putAll(loadResByDirect("A_D",startNo+200,6,wil));
        //下左(D)
        attackMap.putAll(loadResByDirect("A_DL",startNo+210,6,wil));
        //左(L)
        attackMap.putAll(loadResByDirect("A_L",startNo+220,6,wil));
        //上左(UL)
        attackMap.putAll(loadResByDirect("A_UL",startNo+230,6,wil));
        aRes.put("ATTACKMAP", attackMap);

        //死亡动画
        HashMap dieMap = new HashMap();
        //上(U)
        dieMap.putAll(loadResByDirect("D_U",startNo+260,10,wil));
        //上右(UR)
        dieMap.putAll(loadResByDirect("D_UR",startNo+270,10,wil));
        //右(R)
        dieMap.putAll(loadResByDirect("D_R",startNo+280,10,wil));
        //下右(DR)
        dieMap.putAll(loadResByDirect("D_DR",startNo+290,10,wil));
        //下(D)
        dieMap.putAll(loadResByDirect("D_D",startNo+300,10,wil));
        //下左(D)
        dieMap.putAll(loadResByDirect("D_DL",startNo+310,10,wil));
        //左(L)
        dieMap.putAll(loadResByDirect("D_L",startNo+320,10,wil));
        //上左(UL)
        dieMap.putAll(loadResByDirect("D_UL",startNo+330,10,wil));
        aRes.put("DIEMAP", dieMap);
        return aRes;
    }


    private static HashMap loadWeaponRes(SurfaceView v, int startNo){
        WIL.GLOBAL_ONLYWIL_MODE = true;
        HashMap aRes = new HashMap();
        //加载角色图片
        if (getWeaponFilePath() == ""){
            InputStream iFile = v.getResources().openRawResource(R.raw.magic);
            writeData2SDCard("lol", "weapon.wil", iFile);
        }
        WIL wil = new WIL(sdCardRoot + File.separator + "lol" + File.separator + "weapon.wil");
        //站立武器动画
        HashMap weaponMap = new HashMap();
        //上(S_U)
        weaponMap.putAll(loadResByDirect("W_S_U",startNo,4,wil));
        //上右(S_UR)
        weaponMap.putAll(loadResByDirect("W_S_UR",startNo +8 ,4,wil));
        //右(S_RS)
        weaponMap.putAll(loadResByDirect("W_S_R",startNo +16,4,wil));
        //下右(S_DR)
        weaponMap.putAll(loadResByDirect("W_S_DR",startNo + 24,4,wil));
        //下(U_D)
        weaponMap.putAll(loadResByDirect("W_S_D",startNo + 32,4,wil));
        //下左(U_DL)
        weaponMap.putAll(loadResByDirect("W_S_DL",startNo + 40,4,wil));
        //左(U_L)
        weaponMap.putAll(loadResByDirect("W_S_L",startNo + 48,4,wil));
        //上左(U_UL)
        weaponMap.putAll(loadResByDirect("W_S_UL",startNo + 56,4,wil));
        return weaponMap;
    }

    private static HashMap loadMagicRes(SurfaceView v, int startNo){
        WIL.GLOBAL_ONLYWIL_MODE = true;
        HashMap aRes = new HashMap();
        //加载角色图片
        if (GetMagicFilePath() == ""){
            InputStream iFile = v.getResources().openRawResource(R.raw.magic);
            writeData2SDCard("lol", "magic.wil", iFile);
        }
        WIL wil = new WIL(sdCardRoot + File.separator + "lol" + File.separator + "magic.wil");

        //魔法攻击动画(方向比人物运动的动画要多，按照顺时针加序号)
        HashMap magicMap = new HashMap();
        //上(S_U)
        magicMap.putAll(loadResByDirect("M_U",startNo,4,wil));
        //上右(S_UR)
        magicMap.putAll(loadResByDirect("M_UR_0",startNo + 10,4,wil));
        magicMap.putAll(loadResByDirect("M_UR_1",startNo + 20,4,wil));
        magicMap.putAll(loadResByDirect("M_UR_2",startNo + 30,4,wil));
        //右(S_RS)
        magicMap.putAll(loadResByDirect("M_R",startNo+40,4,wil));
        //下右(S_DR)
        magicMap.putAll(loadResByDirect("M_DR_0",startNo+50,4,wil));
        magicMap.putAll(loadResByDirect("M_DR_1",startNo+60,4,wil));
        magicMap.putAll(loadResByDirect("M_DR_2",startNo+70,4,wil));
        //下(U_D)
        magicMap.putAll(loadResByDirect("M_D",startNo+80,4,wil));
        //下左(U_DL)
        magicMap.putAll(loadResByDirect("M_DL_0",startNo+90,4,wil));
        magicMap.putAll(loadResByDirect("M_DL_1",startNo+100,4,wil));
        magicMap.putAll(loadResByDirect("M_DL_2",startNo+110,4,wil));
        //左(U_L)
        magicMap.putAll(loadResByDirect("M_L",startNo+120,4,wil));
        //上左(U_UL)
        magicMap.putAll(loadResByDirect("M_UL_0",startNo+130,4,wil));
        magicMap.putAll(loadResByDirect("M_UL_1",startNo+140,4,wil));
        magicMap.putAll(loadResByDirect("M_UL_2",startNo+150,4,wil));
        return magicMap;
    }
    private static HashMap loadActorRes(SurfaceView v){
        WIL.GLOBAL_ONLYWIL_MODE = true;
        HashMap aRes = new HashMap();
        //加载角色图片
        if (getDefaultFilePath() == ""){
            InputStream iFile = v.getResources().openRawResource(R.raw.hum);
            writeData2SDCard("lol", "hum.wil", iFile);
        }
        WIL wil = new WIL(sdCardRoot + File.separator + "lol" + File.separator + "hum.wil");
        int startNo = 6600;
        if (ChrSelView.curRole == "TS"){
            startNo = 6600;
            aRes.put("MAGIC1", scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.ts369),0.8f));
            aRes.put("MAGIC2", scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.ts375),0.8f));
            aRes.put("MAGIC3", scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.ts388),0.8f));
        }else if (ChrSelView.curRole == "EM"){
            startNo = 9000;
            //三个技能的logo
            aRes.put("MAGIC1", scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.em328),0.8f));
            aRes.put("MAGIC2", scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.em327),0.8f));
            aRes.put("MAGIC3", scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.em320),0.8f));
        }
        loadPercenter = 80;
        //站立动画
        HashMap standMap = new HashMap();
        //上(S_U)
        standMap.putAll(loadResByDirect("S_U",startNo,4,wil));
        //上右(S_UR)
        standMap.putAll(loadResByDirect("S_UR",startNo +8 ,4,wil));
        //右(S_RS)
        standMap.putAll(loadResByDirect("S_R",startNo +16,4,wil));
        //下右(S_DR)
        standMap.putAll(loadResByDirect("S_DR",startNo + 24,4,wil));
        //下(U_D)
        standMap.putAll(loadResByDirect("S_D",startNo + 32,4,wil));
        //下左(U_DL)
        standMap.putAll(loadResByDirect("S_DL",startNo + 40,4,wil));
        //左(U_L)
        standMap.putAll(loadResByDirect("S_L",startNo + 48,4,wil));
        //上左(U_UL)
        standMap.putAll(loadResByDirect("S_UL",startNo + 56,4,wil));

        standMap.putAll(loadWeaponRes(v, 15608));
        aRes.put("STANDMAP", standMap);
        //运动动画
        HashMap runMap = new HashMap();
        //上(U)
        runMap.putAll(loadResByDirect("R_U",startNo + 128,6,wil));
        //上右(UR)
        runMap.putAll(loadResByDirect("R_UR",startNo + 136,6,wil));
        //右(R)
        runMap.putAll(loadResByDirect("R_R",startNo + 144,6,wil));
        //下右(DR)
        runMap.putAll(loadResByDirect("R_DR",startNo +152,6,wil));
        //下(D)
        runMap.putAll(loadResByDirect("R_D",startNo + 160,6,wil));
        //下左(D)
        runMap.putAll(loadResByDirect("R_DL",startNo + 168,6,wil));
        //左(L)
        runMap.putAll(loadResByDirect("R_L",startNo + 176,6,wil));
        //上左(UL)
        runMap.putAll(loadResByDirect("R_UL",startNo + 184,6,wil));
        aRes.put("RUNMAP", runMap);
        //攻击动画
        HashMap attackMap = new HashMap();
        //上(A_U)
        attackMap.putAll(loadResByDirect("A_U",startNo + 392,6,wil));
        //上右(A_UR)
        attackMap.putAll(loadResByDirect("A_UR",startNo + 400,6,wil));
        //右(A_RS)
        attackMap.putAll(loadResByDirect("A_R",startNo + 408,6,wil));
        //下右(A_DR)
        attackMap.putAll(loadResByDirect("A_DR",startNo + 416,6,wil));
        //下(A_D)
        attackMap.putAll(loadResByDirect("A_D",startNo + 424,6,wil));
        //下左(A_DL)
        attackMap.putAll(loadResByDirect("A_DL",startNo + 432,6,wil));
        //左(A_L)
        attackMap.putAll(loadResByDirect("A_L",startNo + 440,6,wil));
        //上左(A_UL)
        attackMap.putAll(loadResByDirect("A_UL",startNo + 448,6,wil));
        aRes.put("ATTACKMAP", attackMap);

        HashMap dieMap = new HashMap();
        //上(A_U)
        dieMap.putAll(loadResByDirect("D_U",startNo + 536,4,wil));
        //上右(A_UR)
        dieMap.putAll(loadResByDirect("D_UR",startNo + 544,4,wil));
        //右(A_RS)
        dieMap.putAll(loadResByDirect("D_R",startNo + 552,4,wil));
        //下右(A_DR)
        dieMap.putAll(loadResByDirect("D_DR",startNo + 560,4,wil));
        //下(A_D)
        dieMap.putAll(loadResByDirect("D_D",startNo + 568,4,wil));
        //下左(A_DL)
        dieMap.putAll(loadResByDirect("D_DL",startNo + 576,4,wil));
        //左(A_L)
        dieMap.putAll(loadResByDirect("D_L",startNo + 584,4,wil));
        //上左(A_UL)
        dieMap.putAll(loadResByDirect("D_UL",startNo + 592,6,wil));
        aRes.put("DIEMAP", dieMap);

        loadPercenter = 90;
        //魔法动画
        HashMap magicMap1 =  loadMagicRes(v, 410);
        aRes.put("MAGICMAP1",magicMap1);
        //HashMap magicMap2 =  loadMagicRes(v, 410);
        //aRes.put("MAGICMAP1",magicMap2);

        HashMap shotMap = new HashMap();
        shotMap.put("A_DEFAULT",scaleBitmap(BitmapFactory.decodeResource(v.getResources(), R.drawable.eshot_0),0.5f));

        //远程普通攻击图片
        aRes.put("ATTACKBMP",shotMap);

        loadPercenter = 100;
        return aRes;
    }
    private static ArrayList<Bitmap> tiledBitmaps = new ArrayList<>();
    private static void loadMap(SurfaceView v){
        if (GetMapTilesFilePath() == ""){
            InputStream iFile = v.getResources().openRawResource(R.raw.tiles);
            writeData2SDCard("lol", "tiles.wil", iFile);
        }
        WIL.GLOBAL_ONLYWIL_MODE = true;
        WIL wil = new WIL(sdCardRoot + File.separator + "lol" + File.separator + "tiles.wil");
        Texture img = wil.tex(516);
        Bitmap bmp = decodeFrameToBitmap(img.getRGBs(), img.getWidth(), img.getHeight());
        tiledBitmaps.add(bmp);

        Texture img1 = wil.tex(552);
        Bitmap bmp1 = decodeFrameToBitmap(img1.getRGBs(), img1.getWidth(), img1.getHeight());
        tiledBitmaps.add(bmp1);

        Texture img2 = wil.tex(571);
        Bitmap bmp2 = decodeFrameToBitmap(img2.getRGBs(), img2.getWidth(), img2.getHeight());
        tiledBitmaps.add(bmp2);

        Texture img3 = wil.tex(572);
        Bitmap bmp3 = decodeFrameToBitmap(img3.getRGBs(), img3.getWidth(), img3.getHeight());
        tiledBitmaps.add(bmp3);

        Texture img4 = wil.tex(565);
        Bitmap bmp4 = decodeFrameToBitmap(img4.getRGBs(), img4.getWidth(), img4.getHeight());
        tiledBitmaps.add(bmp4);

        Texture img5 = wil.tex(550);
        Bitmap bmp5 = decodeFrameToBitmap(img5.getRGBs(), img5.getWidth(), img5.getHeight());
        tiledBitmaps.add(bmp5);
        loadPercenter = 30;
    }

    //Tiled Map Info
    public static int [][]mMapView = {
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}
    };

    /**
     * 根据ID绘制一个tile块
     * @param id
     * @param canvas
     * @param paint
     */
    private void DrawMapTile(int id,Canvas canvas,Paint paint ,int bitMapIndex,int x, int y) {
        canvas.drawBitmap(tiledBitmaps.get(bitMapIndex), x, y,paint);
    }

    private void DrawMap(Canvas canvas,Paint paint) {
        //屏幕的左上角在整个地图中的坐标
        int top = actor.getY() - SHeight/2;
        int left = actor.getX() -SWidth/2;

        // 画面在整个地图中的left和top位置
        int deltaTop = Math.round(top % TILE_HEIGHT);
        int deltaLeft = Math.round(left % TILE_WIDTH);

        int HStartCount = top > 0? top / TILE_HEIGHT : 0;
        int WStartCount = left > 0? left / TILE_WIDTH : 0;

        int iHCount = 0;
        for(int i = HStartCount; i< (HStartCount + SHCount + 1); i++) {
            if (i < TILE_HEIGHT_COUNT ){
                int iWCount = 0;
                for(int j = WStartCount; j<(WStartCount + SWCount + 1) ;j++) {
                    if (j < TILE_WIDTH_COUNT ){
                        int ViewID =  mMapView[i][j];
                        //绘制地图第一层
                        if(ViewID > TILE_NULL) {
                            DrawMapTile(ViewID,canvas,paint,(ViewID-1), iWCount * TILE_WIDTH - deltaLeft, iHCount * TILE_HEIGHT - deltaTop);
                        }
                        iWCount ++;
                    }
                }
                iHCount ++;
            }
        }
    }

    /**
     * 按比例缩放图片
     *
     * @param origin 原图
     * @param ratio  比例
     * @return 新的bitmap
     */
    private static Bitmap scaleBitmap(Bitmap origin, float ratio) {
        if (origin == null) {
            return null;
        }
        int width = origin.getWidth();
        int height = origin.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(ratio, ratio);
        Bitmap newBM = Bitmap.createBitmap(origin, 0, 0, width, height, matrix, false);
        if (newBM.equals(origin)) {
            return newBM;
        }
        origin.recycle();
        return newBM;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //初始化高度和宽度
        SWidth = this.getWidth();
        SHeight = this.getHeight();
        CenterX = SWidth/2;
        CenterY = SHeight/2;
        //初始化当前屏幕可以画的地图的高度和宽度
        SWCount = (SWidth / TILE_WIDTH) + 1;
        SHCount = (SHeight / TILE_HEIGHT) + 1;
        handler = MainActivity.CurActivity.getHandler();
        mainT = new Thread(this);
        mainT.start();
        isRunning = true;
    }


    public static int convertByteToInt(byte data){
        int heightBit = (int) ((data>>4) & 0x0F);
        int lowBit = (int) (0x0F & data);
        return heightBit * 16 + lowBit;
    }

    public static int[] convertByteToColor(byte[] data){
        int size = data.length;
        if (size == 0){
            return null;
        }

        int arg = 0;
        if (size % 3 != 0){
            arg = 1;
        }

        int []color = new int[size / 3 + arg];
        int red, green, blue;

        if (arg == 0){
            for(int i = 0; i < color.length; ++i){
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);
                if (red ==0 && green ==0 && blue==0){
                    color[i] = (red << 16) | (green << 8) | blue | 0x00000000;
                }else{
                    color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
                }

            }
        }else{
            for(int i = 0; i < color.length - 1; ++i){
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);
                if (red ==0 && green ==0 && blue==0){
                    color[i] = (red << 16) | (green << 8) | blue | 0x00000000;
                }else{
                    color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
                }
            }

            color[color.length - 1] = 0xFF000000;
        }

        return color;
    }
    static Bitmap decodeFrameToBitmap(byte[] frame, int w, int h)
    {
        int []colors = convertByteToColor(frame);
        if (colors == null){
            return null;
        }
        Bitmap bmp = Bitmap.createBitmap(colors, 0, w, w, h,Bitmap.Config.ARGB_8888);
        return bmp;
    }

    /**
     * 获取默认的文件路径
     *
     * @return
     */
    public static String getDefaultFilePath() {
        String filepath = "";
        File file = new File(sdCardRoot + File.separator + "lol" + File.separator, "hum.wil");
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        }
        return filepath;
    }

    public static String getMonster5FilePath() {
        String filepath = "";
        File file = new File(sdCardRoot + File.separator + "lol" + File.separator, "mon5.wil");
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        }
        return filepath;
    }

    public static String getDefaultInfoFilePath() {
        String filepath = "";
        File file = new File(sdCardRoot + File.separator + "lol" + File.separator, "huminfo.wix");
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        }
        return filepath;
    }

    public static String getWeaponFilePath() {
        String filepath = "";
        File file = new File(sdCardRoot + File.separator + "lol" + File.separator, "weapon.wil");
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        }
        return filepath;
    }


    public static String GetMapTilesFilePath(){
        String filepath = "";
        File file = new File(sdCardRoot + File.separator + "lol" + File.separator, "tiles.wil");
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        }
        return filepath;
    }

    public static String GetMagicFilePath(){
        String filepath = "";
        File file = new File(sdCardRoot + File.separator + "lol" + File.separator, "magic.wil");
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        }
        return filepath;
    }

    /**
     * 在SD卡上创建目录
     */
    public static File createDirOnSDCard(String dir)
    {
        File dirFile = new File(sdCardRoot + File.separator + dir +File.separator);
        dirFile.mkdirs();
        return dirFile;
    }

    /**
     * 在SD卡上创建文件
     */
    public static File createFileOnSDCard(String fileName, String dir) throws IOException
    {
        File file = new File(sdCardRoot + File.separator + dir + File.separator + fileName);
        file.createNewFile();
        return file;
    }

    /* 写入数据到SD卡中
     */
    public static File writeData2SDCard(String path, String fileName, InputStream data)
    {
        File file = null;
        OutputStream output = null;

        try {
            createDirOnSDCard(path);  //创建目录
            file = createFileOnSDCard(fileName, path);  //创建文件
            output = new FileOutputStream(file);
            byte buffer[] = new byte[2*1024];          //每次写2K数据
            int temp;
            while((temp = data.read(buffer)) != -1 )
            {
                output.write(buffer,0,temp);
            }
            output.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try {
                output.close();    //关闭数据流操作
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return file;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;
    }

    @Override
    public void run() {
        while (isRunning) {
            drawView();
        }
    }

    private void drawMonster(Canvas c, Paint p){
        for(int i=0;i<monsters.size();i++){
            Monster m = monsters.get(i);
            int deltaX = m.getX() - actor.getX();
            int deltaY = m.getY() - actor.getY();
            if (Math.abs(deltaX) < SWidth/2 && Math.abs(deltaY) < SHeight/2){
                m.Run(canvas, CenterX + deltaX, CenterY + deltaY);
            }
        }
    }
    private void drawView() {
        try
        {
            if (holder != null)
            {
                Paint p = new Paint();
                canvas = holder.lockCanvas();
                DrawMap(canvas,p);
                drawMonster(canvas,p);
                //画当前角色
                actor.Run(canvas, CenterX, CenterY);

                p.setColor(Color.WHITE);
                p.setAlpha(100);
                p.setTextSize(20);
                p.setStrokeWidth(2);
                //画方向控制轮盘
                canvas.drawOval(50,SHeight - 500, 500,SHeight - 50,p);
                p.setTextSize(100);
                p.setColor(Color.GRAY);
                canvas.drawOval(200,SHeight - 350, 350,SHeight - 200,p);
                canvas.drawText("↑",220,SHeight - 380,p);
                canvas.drawText("←",70,SHeight - 250,p);
                canvas.drawText("→",380,SHeight - 250,p);
                canvas.drawText("↓",220,SHeight - 100,p);



                //画普通攻击按钮
                p.setColor(Color.WHITE);
                //画中心点（作为参考）
                canvas.drawOval(CenterX -5, CenterY - 5, CenterX+5, CenterY +5, p);

                //debugger;提示坐标信息，便于调试
                //canvas.drawText("X:"+actor.getX()+", Y:"+actor.getY(),220,SHeight - 100,p);
                p.setAlpha(100);
                canvas.drawOval(SWidth -300, SHeight - 300, SWidth-50, SHeight - 50, p);

                canvas.drawOval(SWidth -480, SHeight - 200, SWidth-360, SHeight - 80, p);
                canvas.drawOval(SWidth -410, SHeight - 400, SWidth-290, SHeight - 280, p);
                canvas.drawOval(SWidth -200, SHeight - 460, SWidth-80, SHeight - 340, p);
                //debugger;提示魔法ID，便于调试
                //canvas.drawText(String.valueOf(actor.getCurMagic()), 200,200,p);
                if (actor.getCurMagic() == 1){
                    canvas.drawOval(SWidth -520, SHeight - 240, SWidth-320, SHeight - 40, p);
                    float cx = SWidth -420;
                    float cy = SHeight -140;
                    drawMagic(canvas, p, cx,cy);
                }else if (actor.getCurMagic() == 2){
                    canvas.drawOval(SWidth -450, SHeight - 440, SWidth-250, SHeight - 240, p);
                    float cx = SWidth -350;
                    float cy = SHeight -340;
                    drawMagic(canvas, p, cx,cy);
                }else if (actor.getCurMagic() == 3){
                    canvas.drawOval(SWidth -240, SHeight - 500, SWidth-40, SHeight - 300, p);
                    float cx = SWidth -140;
                    float cy = SHeight -400;
                    drawMagic(canvas, p, cx,cy);
                }

                p.setAlpha(255);
                canvas.drawBitmap(bmpAttr,SWidth -240, SHeight - 250,p);

                canvas.drawBitmap(actor.getMagic1(),SWidth -480 + (120 - actor.getMagic1().getWidth())/2, SHeight - 200 + (120 - actor.getMagic1().getHeight())/2,p);
                canvas.drawBitmap(actor.getMagic2(),SWidth -410 + (120 - actor.getMagic2().getWidth())/2, SHeight - 400 + (120 - actor.getMagic2().getHeight())/2,p);
                canvas.drawBitmap(actor.getMagic3(),SWidth -200 + (120 - actor.getMagic3().getWidth())/2, SHeight - 460 + (120 - actor.getMagic3().getHeight())/2,p);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            if (canvas != null)
                holder.unlockCanvasAndPost(canvas);
        }
    }

    private void drawMagic(Canvas c, Paint p, float cx, float cy){
        if (!actor.getFireMagic()){
            p.setColor(Color.WHITE);
            p.setAlpha(100);
            p.setStrokeWidth(100);
            float dx = magicOffsetX - cx;
            float dy = magicOffsetY - cy;
            double dzz = Math.sqrt(dx*dx + dy*dy);
            canvas.drawLine(CenterX + Actor.w/2, CenterY + Actor.h/2, CenterX + Actor.w/2 + (float)(600* dx/dzz), CenterY + Actor.h/2 + (float)(600*dy/dzz), p);
        }
    }

    public static void PlaySoundById(int id){
        Message msg = new Message();
        msg.arg1 = id;
        handler.sendMessage(msg);
    }

    private long preMilSecond = 0;
    //移动事件
    private void moveEvent(float ex,float ey){
        if (ex >200 && ex < 350 && ey >SHeight-500 && ey < SHeight-350){
            actor.setRun(true);
            actor.setDirect("U");
        }else if (ex >200 && ex < 350 && ey >SHeight-200 && ey <SHeight-50){
            actor.setRun(true);
            actor.setDirect("D");
        }else if (ex >50 && ex < 200 && ey >SHeight-350 && ey <SHeight-200){
            actor.setRun(true);
            actor.setDirect("L");
        }else if (ex >350 && ex < 500 && ey >SHeight-350 && ey <SHeight-200){
            actor.setRun(true);
            actor.setDirect("R");
        }
        else if (ex >50 && ex < 200 && ey >SHeight-500 && ey <SHeight-350){
            actor.setRun(true);
            actor.setDirect("UL");
        }
        else if (ex >350 && ex < 500 && ey >SHeight-500 && ey <SHeight-350){
            actor.setRun(true);
            actor.setDirect("UR");
        }
        else if (ex >50 && ex < 200 && ey >SHeight-200 && ey <SHeight-50){
            actor.setRun(true);
            actor.setDirect("DL");
        }
        else if (ex >350 && ex < 500 && ey >SHeight-200 && ey <SHeight-50){
            actor.setRun(true);
            actor.setDirect("DR");
        }
        if (actor.getRun()){
            long curMilSecode = System.currentTimeMillis();
            if (preMilSecond == 0){
                PlaySoundById(1);
                preMilSecond = System.currentTimeMillis();
            }else if (curMilSecode - preMilSecond >700){
                PlaySoundById(1);
                preMilSecond = System.currentTimeMillis();
            }
        }
    }
    //停止移动事件
    private void stopMoveEvent(float ex, float ey){
        actor.setRun(false);
        if (ex >200 && ex < 350 && ey >SHeight-500 && ey < SHeight-350){
            actor.setDirect("U");
        }else if (ex >200 && ex < 350 && ey >SHeight-200 && ey <SHeight-50){
            actor.setDirect("D");
        }else if (ex >50 && ex < 200 && ey >SHeight-350 && ey <SHeight-200){
            actor.setDirect("L");
        }else if (ex >350 && ex < 500 && ey >SHeight-350 && ey <SHeight-200){
            actor.setDirect("R");
        }
        else if (ex >50 && ex < 200 && ey >SHeight-500 && ey <SHeight-350){
            actor.setDirect("UL");
        }
        else if (ex >350 && ex < 500 && ey >SHeight-500 && ey <SHeight-350){
            actor.setDirect("UR");
        }
        else if (ex >50 && ex < 200 && ey >SHeight-200 && ey <SHeight-50){
            actor.setDirect("DL");
        }
        else if (ex >350 && ex < 500 && ey >SHeight-200 && ey <SHeight-50){
            actor.setDirect("DR");
        }else {
            actor.setRun(false);
        }
    }

    private void stopDrawRangeEvent(float ex, float ey){
        actor.setDrawRange(false);
    }
    //攻击事件
    private void attackEvent(float ex, float ey){
        if (actor.getCurMagic() == 0){
            if (ex >SWidth -300 && ex < SWidth - 50 && ey >SHeight - 300 && ey < SHeight - 50){
                actor.setDrawRange(true);
                if (!actor.getRun()){
                    float curDefalutX = actor.getX() + Actor.w/2;
                    float curDefaultY = actor.getY() - 30;
                    if (actor.getAttactFinish()){
                        actor.createShot(curDefalutX, curDefaultY, actor.getDirect());
                        actor.setAttact(true);
                        actor.setAttactFinish(false);
                    }
                }
            }
        }

    }

    private void setMagicEvent(float ex, float ey){
        if (!actor.getFireMagic()){
            if (ex > SWidth -480 && ey > SHeight - 200 && ex <SWidth-360 && ey <SHeight - 80){
                actor.setCurMagic(1);
            }else if (ex > SWidth -410 && ey > SHeight - 400 && ex <SWidth-290 && ey <SHeight - 280){
                actor.setCurMagic(2);
            }else if (ex > SWidth -200 && ey > SHeight - 460 && ex <SWidth-80 && ey <SHeight - 340){
                actor.setCurMagic(3);
            }else{
                actor.setCurMagic(0);
            }
        }
    }

    private void moveMagicEvent(float ex, float ey){
        if (!actor.getFireMagic()){
            if (ex > SWidth /2 ){
                magicOffsetX = ex;
                magicOffsetY = ey;
                if (actor.getCurMagic() == 1){
                    magicStartX =SWidth -420;
                    magicStartY =SHeight -140;
                }else if (actor.getCurMagic() == 2){
                    magicStartX =SWidth -350;
                    magicStartY =SHeight -340;
                }else if (actor.getCurMagic() == 3){
                    magicStartX =SWidth -140;
                    magicStartY =SHeight -400;
                }else{
                    magicStartX = 0;
                    magicStartY = 0;
                }
            }
        }
    }

    private void fireMagicEvent(float ex, float ey){
        if (!actor.getFireMagic()){
            if (ex > SWidth /2 && actor.getCurMagic() !=0){
                //设置
                magicDir = CollisionUtil.getNextDirection(magicStartX, magicStartY, magicOffsetX, magicOffsetY);
                actor.setCurMagic(0);
                actor.setDirect(magicDir);
                actor.setAttact(true);
                actor.setFireMagic(true);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int pointerCount = event.getPointerCount();
        //int actionType = event.getActionMasked();
        int actionType = (event.getAction()&MotionEvent.ACTION_MASK) % 5;//统一单点和多点
        switch (actionType){
            case MotionEvent.ACTION_MOVE:{
                for(int i=0;i<pointerCount;i++){
                    float ex = event.getX(i);
                    float ey = event.getY(i);
                    moveEvent(ex,ey);
                    attackEvent(ex,ey);
                    moveMagicEvent(ex,ey);
                }
                break;
            }
            case MotionEvent.ACTION_UP:{
                for(int i=0;i<pointerCount;i++){
                    float ex = event.getX(i);
                    float ey = event.getY(i);
                    stopMoveEvent(ex,ey);
                    stopDrawRangeEvent(ex,ey);
                    fireMagicEvent(ex,ey);
                }
                break;
            }
            case MotionEvent.ACTION_DOWN: {
                for(int i=0;i<pointerCount;i++){
                    float ex = event.getX(i);
                    float ey = event.getY(i);
                    setMagicEvent(ex,ey);
                }
                break;
            }
            case MotionEvent.ACTION_POINTER_DOWN:{
                for(int i=0;i<pointerCount;i++){
                    float ex = event.getX(i);
                    float ey = event.getY(i);
                    moveEvent(ex,ey);
                    attackEvent(ex,ey);
                    setMagicEvent(ex,ey);
                }
                break;
            }
            case MotionEvent.ACTION_POINTER_UP:{
                for(int i=0;i<pointerCount;i++){
                    float ex = event.getX(i);
                    float ey = event.getY(i);
                    moveEvent(ex,ey);
                    attackEvent(ex,ey);
                    fireMagicEvent(ex,ey);
                }
                break;
            }
        }
        return true;
    }
}
