package com.example.she.lol.comom;
import android.graphics.RectF;
/**
 * Created by she on 2017/11/22.
 */

public class CollisionUtil {
    /**
     * 矩形碰撞检测 参数为x,y,width,height
     *
     * @param x1
     * 			      第一个矩形的x
     * @param y1
     *            第一个矩形的y
     * @param w1
     *            第一个矩形的w
     * @param h1
     *            第一个矩形的h
     * @param x2
     *            第二个矩形的x
     * @param y2
     *            第二个矩形的y
     * @param w2
     *            第二个矩形的w
     * @param h2
     *            第二个矩形的h
     * @return 是否碰撞
     */
    public static boolean IsRectCollision(float x1, float y1, float w1, float h1,  float x2,  float y2, float w2, float h2) {
        if (x2 > x1 && x2 > x1 + w1) {
            return false;
        } else if (x2 < x1 && x2 < x1 - w2) {
            return false;
        } else if (y2 > y1 && y2 > y1 + h1) {
            return false;
        } else if (y2 < y1 && y2 < y1 - h2) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 矩形碰撞检测 参数为Rect对象
     *
     * @param r1
     *            第一个Rect对象
     * @param r2
     *            第二个Rect对象
     * @return 是否碰撞
     */
    public static boolean IsRectCollision(RectF r1, RectF r2) {
        return IsRectCollision(r1.left, r1.top, r1.right - r1.left, r1.bottom
                - r1.top, r2.left, r2.top, r2.right - r2.left, r2.bottom
                - r2.top);
    }

    public static String getNextDirection(float mx, float my, float ax, float ay){
        String ret = "D";
        float flagx = 0;
        if (mx < ax) {flagx = 1;}
        else if (mx == ax) {flagx = 0;}
        else {flagx = -1;}
        if (Math.abs(my-ay) > 2){ if ((mx >= ax-1) && (mx <= ax+1)) {flagx = 0;}}
        float flagy = 0;
        if (my < ay){ flagy = 1;}
        else if (my == ay){ flagy = 0;}
        else {flagy = -1;}
        if (Math.abs(mx-ax) > 2) { if ((my > ay-1) && (my <= ay+1)) { flagy = 0;}}

        if ((flagx == 0) && (flagy == -1)) { ret = "U";}
        if ((flagx == 1) && (flagy == -1)) { ret = "UR";}
        if ((flagx == 1) && (flagy == 0))  { ret = "R";}
        if ((flagx == 1) && (flagy == 1))  { ret = "DR";}
        if ((flagx == 0) && (flagy == 1))  { ret = "D";}
        if ((flagx == -1) && (flagy == 1))  { ret = "DL";}
        if ((flagx == -1) && (flagy == 0))  { ret = "L";}
        if ((flagx == -1) && (flagy == -1)) { ret = "UL";}
        return ret;
    }
}
