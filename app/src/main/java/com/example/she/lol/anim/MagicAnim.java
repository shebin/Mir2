package com.example.she.lol.anim;


import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.example.she.lol.Actor;
import com.example.she.lol.GameView;
import com.example.she.lol.Monster;
import com.example.she.lol.comom.CollisionUtil;
import com.github.jootnet.mir2.core.image.ImageInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by she on 2017/1/10.
 */

public class MagicAnim {
    private long deltaTime;	//多长时间跳一帧
    private int mCount;	//一共多少帧
    private int curM = 0;	//当前帧数
    private long dz;	//当前帧的计时器
    private long preTime;	//记录的上一帧的执行时间
    private HashMap bmpRes;
    private String dir;
    private float sx;
    private float sy;
    private float dx;
    private float dy;
    private float z;
    private int moveCount;

    public MagicAnim(HashMap bmpRes){
        this.deltaTime = 50;	//多少毫秒跳一帧
        this.dz = 0;
        this.preTime = 0;
        this.bmpRes = bmpRes;
        this.mCount = 4;
        this.moveCount = 0;
    }

    private String getMagicFlagByDir(String dir){
        String tmpFlag = dir;
        if (!(dir == "U" || dir == "D" || dir == "L" || dir == "R")){
            tmpFlag = tmpFlag + "_1";   //实际上有三个方向，现在先写死，取中间那个
        }
        return  "M_" + tmpFlag;
    }


    //把当前的画布传进来,并且把坐标传进来
    public void Run(Canvas c, String dir, float sx1, float sy1, float dx1, float dy1, float l, float t){
        this.dir = dir;
        this.sx = sx1;
        this.sy = sy1;
        this.dx = dx1;
        this.dy = dy1;
        this.z = (float)Math.sqrt((dx1 - sx1)*(dx1 - sx1) + (dy1-sy1)*(dy1-sy1));

        //角色
        String FLAG = getMagicFlagByDir(dir);
        ArrayList<Bitmap> curImages = (ArrayList<Bitmap>)bmpRes.get(FLAG);
        Bitmap curImg = curImages.get(curM);
        ArrayList<ImageInfo> curImgInfos = (ArrayList<ImageInfo>)bmpRes.get(FLAG+"_INFO");
        ImageInfo curImgInfo = curImgInfos.get(curM);

        long curTime = System.currentTimeMillis();
        dz = curTime - preTime;

        if (dz > deltaTime){
            moveCount++;
            curM  = curM +1;
            if (curM >= mCount){
                curM = 0;
            }
            dz = 0;
            preTime = curTime;
        }
        float distance = moveCount *40;
        float xds = (dx-sx) * distance/z;
        float yds = (dy-sy) * distance/z;
        c.drawBitmap(curImg, l+curImgInfo.getOffsetX()  + xds,t + curImgInfo.getOffsetY()+ yds, null);

        //碰撞检测，看当前的魔法跟那些怪物碰撞，暂时不消失
        for(int i=0;i<GameView.monsters.size();i++){
            Monster curMon = GameView.monsters.get(i);
            if (curMon.getDie()){ continue; }
            int deltaX = curMon.getX() - GameView.actor.getX();
            int deltaY = curMon.getY() - GameView.actor.getY();
            if (CollisionUtil.IsRectCollision(l+curImgInfo.getOffsetX()  + xds, t + curImgInfo.getOffsetY()+ yds, curImg.getWidth(),curImg.getHeight(), GameView.CenterX + deltaX, GameView.CenterY + deltaY, curMon.getW(), curMon.getH())){
                if (curMon.getHp() >=0){
                    int tmpValue = curMon.getHp() - 200;
                    if (tmpValue <0){
                        tmpValue =0;
                        curMon.die();
                    }else{
                        curMon.setTarget(true);
                    }
                    this.moveCount = 0;
                    GameView.actor.setFireMagic(false);
                    curMon.setHp(tmpValue);
                }
            }
        }

        if (distance > 500){
            this.moveCount = 0;
            GameView.actor.setFireMagic(false);
        }
    }
}
