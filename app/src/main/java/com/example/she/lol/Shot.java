package com.example.she.lol;

import android.graphics.Canvas;

import com.example.she.lol.anim.AttackEffectAnim;
import com.example.she.lol.comom.CollisionUtil;

import java.util.HashMap;

/**
 * Created by she on 2017/11/21.
 */

public class Shot {
    private float x;  //实时的X
    private float y;  //实时的Y
    private String direct;
    private Actor a;
    private Monster m;  //子弹目前攻击的目标
    private float startLeft;
    private int curM;
    private static float delX = 30;
    private static float delY = 30;
    private static float delXX = 21;
    private static float delYX = 21;

    private float defaultX; //发出时候的X
    private float defaultY; //发出时候的Y

    private Boolean isHit;  //本次是否命中

    public Boolean getHit() {
        return isHit;
    }

    public void setHit(Boolean hit) {
        isHit = hit;
    }

    public float getDefaultX() {
        return defaultX;
    }

    public void setDefaultX(float defaultX) {
        this.defaultX = defaultX;
        //this.x = defaultX;
    }

    public float getDefaultY() {
        return defaultY;
    }

    public void setDefaultY(float defaultY) {
        this.defaultY = defaultY;
        //this.y = defaultY;
    }

    public int getCurM() {
        return curM;
    }

    public void setCurM(int curM) {
        this.curM = curM;
    }

    public float getStartLeft() {
        return startLeft;
    }

    public void setStartLeft(float startLeft) {
        this.startLeft = startLeft;
    }

    public float getStartTop() {
        return startTop;
    }

    public void setStartTop(float startTop) {
        this.startTop = startTop;
    }

    private float startTop;
    private AttackEffectAnim attackEffectAnim;

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public Actor getA() {
        return a;
    }

    public void setA(Actor a) {
        this.a = a;
    }

    public Monster getM() {
        return m;
    }

    public void setM(Monster m) {
        this.m = m;
    }

    public AttackEffectAnim getAttackEffectAnim() {
        return attackEffectAnim;
    }

    public void setAttackEffectAnim(AttackEffectAnim attackEffectAnim) {
        this.attackEffectAnim = attackEffectAnim;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public double getShotDistance(){
        float _x = Math.abs(this.defaultX - this.x);
        float _y = Math.abs(this.defaultY - this.y);
        return Math.sqrt(_x*_x+_y*_y);
    }

    public float getNextX(){
        float ret = this.getX();
        if (m == null){
            if (direct == "L"){
                ret = this.getX() - delX;
            }else if (direct == "R"){
                ret = this.getX() + delX;
            }else if (direct == "DR" || direct == "UR"){
                ret = this.getX() + delXX;
            }else if (direct == "DL" || direct == "UL"){
                ret = this.getX() - delXX;
            }
        }else{
            float distanceX = this.defaultX - m.getX();
            ret = this.getX() - distanceX/6;
        }

        return ret;
    }

    public float getNextY(){
        float ret = this.getY();
        if (m == null){
            if (direct == "U"){
                ret = this.getY() - delY;
            }else if (direct == "D"){
                ret = this.getY() + delY;
            }else if (direct == "UL" || direct == "UR"){
                ret = this.getY() - delYX;
            }else if (direct == "DL" || direct == "DR"){
                ret = this.getY() + delYX;
            }
        }else{
            float distanceY = this.defaultY - m.getY();
            ret = this.getY() - distanceY/6;
        }

        return ret;
    }

    public double getMosterDistance(Monster parM){
        float _x = Math.abs(this.defaultX - (parM.getX() + parM.getW()/2));
        float _y = Math.abs(this.defaultY - (parM.getY() + parM.getH()));
        return Math.sqrt(_x*_x+_y*_y);
    }

    public void setTargetMonster(){
        this.m = null;
        double mind = 1000;
        for (int i=0;i<GameView.monsters.size();i++){
            Monster curM = GameView.monsters.get(i);
            curM.setTarget(false);
            if (!curM.getDie()){
                double d = getMosterDistance(curM);
                if (d <=350 && d< mind){
                    this.m = curM;
                    mind = d;
                }
            }
        }
        if (this.m != null){
            this.m.setTarget(true);
        }
    }

    public Boolean isCollision(){
        Boolean ret = false;
        if (this.m != null){
            ret = CollisionUtil.IsRectCollision(this.getX(), this.getY(), 30,30, this.m.getX(), this.m.getY(), this.m.getW(), this.m.getH());
        }
        return ret;
    }

    public Shot(HashMap bmpRes){
        this.isHit = false;
        this.setCurM(0);
        this.attackEffectAnim = new AttackEffectAnim(bmpRes);
    }
}
