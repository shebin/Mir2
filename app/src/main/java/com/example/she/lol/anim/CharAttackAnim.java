package com.example.she.lol.anim;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.she.lol.Actor;
import com.example.she.lol.GameView;
import com.github.jootnet.mir2.core.image.ImageInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by she on 2017/1/10.
 */

public class CharAttackAnim {
    private long deltaTime;	//多长时间跳一帧
    private int mCount;	//一共多少帧
    private int curM = 0;	//当前帧数
    private long dz;	//当前帧的计时器
    private long preTime;	//记录的上一帧的执行时间
    private HashMap bmpRes;

    public CharAttackAnim(HashMap bmpRes){
        this.deltaTime = 150;	//多少毫秒跳一帧
        this.dz = 0;
        this.preTime = 0;
        this.bmpRes = bmpRes;
        this.mCount = 6;
    }
    //把当前的画布传进来,并且把坐标传进来
    public void Run(Canvas c, Actor m,  float l, float t){
        //角色
        String FLAG = "A_"+m.getDirect();
        ArrayList<Bitmap> curImages = (ArrayList<Bitmap>)bmpRes.get(FLAG);
        Bitmap curImg = curImages.get(curM);
        ArrayList<ImageInfo> curImgInfos = (ArrayList<ImageInfo>)bmpRes.get(FLAG+"_INFO");
        ImageInfo curImgInfo = curImgInfos.get(curM);
        c.drawBitmap(curImg, l+curImgInfo.getOffsetX(),t + curImgInfo.getOffsetY(), null);

        Paint p = new Paint();
        p.setColor(Color.RED);
        c.drawRect(l,t - Actor.h/2 ,l+ Actor.w,t - Actor.h/2 +10,p);

        float percent = (float)m.getHp()/(float)m.getMaxhp();
        p.setColor(Color.GREEN);
        c.drawRect(l,t - Actor.h/2 ,l+ Actor.w * percent,t - Actor.h/2 +10,p);

        long curTime = System.currentTimeMillis();
        dz = curTime - preTime;
        if (dz > deltaTime){
            curM  = curM +1;
            if (curM >= mCount){
                curM = 0;
                m.setAttact(false);
            }
            dz = 0;
            preTime = curTime;
        }
    }
}
