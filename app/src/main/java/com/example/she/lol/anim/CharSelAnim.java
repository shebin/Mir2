package com.example.she.lol.anim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import java.util.ArrayList;

/**
 * Created by she on 2017/1/10.
 */

public class CharSelAnim {
    private ArrayList<Bitmap> animBitmap;	//动画图片
    private long startTime;	//开始时间
    //private long durationTime;	//持续时间
    private long deltaTime;	//多长时间跳一帧
    private int mCount;	//一共多少帧
    private int curM = 0;	//当前帧数
    private long dz;	//当前帧的计时器
    private long preTime;	//记录的上一帧的执行时间

    private Context context;

    public CharSelAnim(Context context, ArrayList<Bitmap> animBMP){
        this.deltaTime = 150;	//多少毫秒跳一帧
        this.dz = 0;
        this.startTime = 0;
        this.preTime = 0;
        this.context = context;
        this.animBitmap = animBMP;
        this.mCount = this.animBitmap.size();
    }

    //把当前的画布传进来,并且把坐标传进来
    public void Run(Canvas c, float l, float t){
        long curTime = System.currentTimeMillis();
        dz = curTime - preTime;
        if (dz > deltaTime){
            curM  = curM +1;
            if (curM >= mCount){
                curM = 0;
            }
            dz = 0;
            preTime = curTime;
        }
        c.drawBitmap(animBitmap.get(curM), l,t, null);
    }
}
