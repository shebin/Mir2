package com.example.she.lol.anim;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.she.lol.Actor;
import com.example.she.lol.GameView;
import com.github.jootnet.mir2.core.image.ImageInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by she on 2017/1/10.
 */

public class CharRunAnim {
    private long deltaTime;	//多长时间跳一帧
    private int mCount;	//一共多少帧
    private int curM = 0;	//当前帧数
    private long dz;	//当前帧的计时器
    private long preTime;	//记录的上一帧的执行时间
    private HashMap bmpRes;

    private static int minPix = 4; //一步走多少个像素

    public CharRunAnim(HashMap bmpRes){
        this.deltaTime = 220;	//多少毫秒跳一帧
        this.dz = 0;
        this.preTime = 0;
        this.bmpRes = bmpRes;
        this.mCount = 6;
    }

    private int getNextMapIndex(Actor m){
        int mapIndex = 0;
        if (m.getDirect() == "U") {
            int iPosX = (m.getX() + Actor.w/2)/GameView.TILE_WIDTH;
            int iPosY = (m.getY() + Actor.h/2 -minPix) /GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }else if (m.getDirect() == "D") {
            int iPosX = (m.getX() + Actor.w/2)/GameView.TILE_WIDTH;
            int iPosY = (m.getY() + Actor.h/2 + minPix) /GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }else if (m.getDirect() == "L") {
            int iPosX = (m.getX()+ Actor.w/2 - minPix)/GameView.TILE_WIDTH;
            int iPosY = (m.getY() + Actor.h/2)/GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }
        else if (m.getDirect() == "R") {
            int iPosX = (m.getX() + Actor.w/2 + minPix)/GameView.TILE_WIDTH;
            int iPosY = (m.getY() + Actor.h/2)/GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }else if (m.getDirect() == "UR") {
            int iPosX = (m.getX()+ Actor.w/2 + 3)/GameView.TILE_WIDTH;
            int iPosY = (m.getY()+ Actor.h/2 - 3)/GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }else if (m.getDirect() == "UL") {
            int iPosX = (m.getX()+ Actor.w/2 - 3)/GameView.TILE_WIDTH;
            int iPosY = (m.getY()+ Actor.h/2 - 3)/GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }else if (m.getDirect() == "DR") {
            int iPosX = (m.getX()+ Actor.w/2 + 3)/GameView.TILE_WIDTH;
            int iPosY = (m.getY()+ Actor.h/2 + 3)/GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }else if (m.getDirect() == "DL") {
            int iPosX = (m.getX()+ Actor.w/2 - 3)/GameView.TILE_WIDTH;
            int iPosY = (m.getY()+ Actor.h/2 + 3)/GameView.TILE_HEIGHT;
            mapIndex = GameView.mMapView[iPosY][iPosX];
        }
        return mapIndex;
    }
    //把当前的画布传进来,并且把坐标传进来
    public void Run(Canvas c, Actor m,  float l, float t){
        //角色
        String FLAG = "R_"+m.getDirect();
        ArrayList<Bitmap> curImages = (ArrayList<Bitmap>)bmpRes.get(FLAG);
        Bitmap curImg = curImages.get(curM);
        ArrayList<ImageInfo> curImgInfos = (ArrayList<ImageInfo>)bmpRes.get(FLAG+"_INFO");
        ImageInfo curImgInfo = curImgInfos.get(curM);
        c.drawBitmap(curImg, l+curImgInfo.getOffsetX(),t + curImgInfo.getOffsetY(), null);

        Paint p = new Paint();
        p.setColor(Color.RED);
        c.drawRect(l,t - Actor.h/2 ,l+ Actor.w,t - Actor.h/2 +10,p);

        float percent = m.getHp()/m.getMaxhp();
        p.setColor(Color.GREEN);
        c.drawRect(l,t - Actor.h/2 ,l+ Actor.w * percent,t - Actor.h/2 +10,p);


        int top = m.getY() + Actor.h/2 - GameView.SHeight/2;
        int left = m.getX() + Actor.w/2 - GameView.SWidth/2;
        int right = GameView.MAP_WIDTH - (m.getX() + Actor.w/2 + GameView.SWidth/2);
        int bottom = GameView.MAP_HEIGHT - (m.getY() + Actor.h/2 + GameView.SHeight/2);

        long curTime = System.currentTimeMillis();
        dz = curTime - preTime;
        if (dz > deltaTime){
            curM  = curM +1;
            if (curM >= mCount){
                curM = 0;
            }
            dz = 0;
            preTime = curTime;
        }

        if (m.getDirect() == "U"){
            if (top > minPix && getNextMapIndex(m) == 2){
                m.setY(m.getY() -minPix);
            }
        }else if (m.getDirect() == "D"){
            if (bottom > minPix && getNextMapIndex(m) == 2){
                m.setY(m.getY() + minPix);
            }
        }else if (m.getDirect() == "L"){
            if (left > minPix && getNextMapIndex(m) == 2){
                m.setX(m.getX() - minPix);
            }
        }else if (m.getDirect() == "R"){
            if (right > minPix && getNextMapIndex(m) == 2){
                m.setX(m.getX() + minPix);
            }
        }else if (m.getDirect() == "UL"){
            if (left > minPix && top >minPix && getNextMapIndex(m) == 2){
                m.setX(m.getX() - 3);
                m.setY(m.getY() - 3);
            }
        }else if (m.getDirect() == "UR"){
            if (right > minPix && top >minPix && getNextMapIndex(m) == 2){
                m.setX(m.getX() + 3);
                m.setY(m.getY() - 3);
            }
        }else if (m.getDirect() == "DL"){
            if (left > minPix && bottom >minPix && getNextMapIndex(m) == 2){
                m.setX(m.getX() - 3);
                m.setY(m.getY() + 3);
            }
        }else if (m.getDirect() == "DR"){
            if (right > minPix && bottom >minPix && getNextMapIndex(m) == 2){
                m.setX(m.getX() + 3);
                m.setY(m.getY() + 3);
            }
        }else{
            if (top > minPix && getNextMapIndex(m) == 2){
                m.setY(m.getY() - minPix);
            }
        }
    }
}
